var TUG = (function() {
    var options = {
        currentsong : "",
        myname : "",
        plugpulse : ""
    };

    /*
        This fires off the init of TUG and basically is the core event
    */
    var init = function() {
        API.on(API.ADVANCE, DJAdvancedHandler);
        API.on(API.CHAT, chatHandler);
        API.on(API.USER_JOIN, joinHandler);
        API.on(API.USER_LEAVE, leaveHandler);
        API.chatLog("Sucessfully loaded TUG!");
    };

    /*
        DJ Adavanced Handler whenever a dj is up next it fires this off
    */
    var DJAdvancedHandler = function() {
        //send ajax requests when dj advances for new song? maybe set timeout for 1-2 seconds for properply wait for lag?
        //maybe check and see last song and also verify it has changed?
    };

    var leaveHandler = function(e) {
        /*
         Map Object below
         avatarID: "classic03"
         badge: "winter05"
         friend: true
         gRole: null
         grab: false
         id: 3575198
         joined: "2013-01-25 00:44:57.228000"
         language: "en"
         level: 14
         priority: 2
         rawun: "whitepowder"
         role: 4
         status: 1
         sub: 0
         uIndex: 1
         username: "whitepowder"
         vote: 0
         */
    };

    var joinHandler = function(e) {
        /*
         Map Object below
         avatarID: "warrior04"
         badge: "subyearly-g"
         friend: false
         gRole: 0
         grab: false
         id: 6165455
         joined: "2015-03-19 17:55:02.815118"
         language: "en"
         level: 8
         priority: 0
         rawun: "Bartwo"
         role: 0
         slug: "bartwo"
         status: 1
         sub: 1
         uIndex: undefined
         username: "Bartwo"
         vote: 0
         */
        var plugJoined = e.joined;
        var userID = e.id;
        var userName = e.username;
        var friend = e.friend;

    };

    /*
        Handle all chat intake here
    */
    var chatHandler = function(e) {
        switch (e.type) {
            case "message" :
                messageHandler(e);
                break;
            default:
                break;
        }
    };

    /*
    Handles user messages
    */
    var messageHandler = function(e)
    {
        var userName = e.un;
        var chatMessage = e.message;
        var chatID = e.cid;
        var timeStamp = e.timestamp;
        var userID = e.uid;

        //do rolls
    }

    return {
        init: init,
        options: options
    };
})();

TUG.init();